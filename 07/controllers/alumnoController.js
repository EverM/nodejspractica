const { leerDB, guardarDB } = require('../helpers/guardarArchivo');
const Alumno = require('../models/alumno');
const Alumnos= require('../models/alumnos');

const { response } = 'express';

const alumnos = new Alumnos();

let alumnoDB = leerDB();
if (alumnoDB) {
  alumnos.cargarAlumnosFromArray(alumnoDB);
}

const alumnoGet = (req, res = response) => {

  res.json({
    msg: 'get API - Controlador',
    alumnoDB
  })
}

const alumnoPost = (req, res = response) => {

  const { nombres, apellidos, cursos } = req.body;
  const alumno = new Alumno(nombres, apellidos, cursos);

  alumnos.crearAlumno(alumno);
  guardarDB(alumnos.listArray);

  const listado = leerDB();
  alumnos.cargarAlumnosFromArray();

  res.json({
    msg: 'cargado pos',
    listado
  })
}

const alumnoPut = (req, res = response) => {
  const { id } = req.params;

  if (id) {
    alumnos.eliminarAlumno(id);
    const { nombres, apellidos, cursos } = req.body;
    const alumno = new Alumno(nombres, apellidos, cursos);

    alumno.getId(id);
    alumnos.crearAlumno(alumno);
    guardarDB(alumnos.listArray);

    alumnoDB = leerDB();
  }


  res.json({
    msg: 'cargado putr',
    alumnoDB
  })
}



const alumnoDelete = (req, res = response) => {

  const { id } = req.params

  if (id) {
    alumnos.eliminarAlumno(id);
    guardarDB(alumnos.listArray)
  }

  res.json({
    msg: 'compra eliminada',
  })
}


module.exports = {
  alumnoGet,
  alumnoPut,
  alumnoPost,
  alumnoDelete
}