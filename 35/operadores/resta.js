//RESTA

const fs = require('fs');
const crearArchivo = (base = 5) => {
  try {
    let salida = '';

    for (let i = 1; i <= 100; i++) {
      salida += `${base} - ${i} = ${base - i}\n`;
    }

    fs.writeFileSync(`tablas/tabla-${base}.txt`, salida);
    console.log(`El archivo tabla-${base}.txt ha sido creado`.cyan);
   } catch (error) {
      
    throw error;
    }
  }
  module.exports = {
    generarArchivoR : crearArchivo
  }
