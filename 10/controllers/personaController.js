const Personas = require("../models/personas");

const axios = require('axios');
const { guardarInfo, leerInfo, guardarInfo2, guardarInfo3 } = require("../models/guardarInfo");
const { response } = require("express");

const url = 'https://pokeapi.co/api/v2/pokemon/';
const url2 = 'https://covidtracking.com/data/api';
const url3 = 'https://jsonplaceholder.typicode.com/todos';

const personas = new Personas()

const lista1 = []
const lista2 = []
const lista3 = []

const pokemonAPiGet = async (req, res = response) => {

  try{
    const jsonResponse = await axios.get(url);
    const response = jsonResponse.data.results;

    Object.keys(response).forEach(key => {
      const poke= response[key]
      lista1.push(poke)
});
  console.log('', lista1, '');
  guardarInfo(lista1)
    
    res.json({
      response
    });
  } catch (error) {
    console.log(error);
    
    res.json({
      error: 'ERROR EN SERVIDOR AL CONSUMIR'
    });
  }

};


const covidTrackingGet = async (req, res = response) => {

  try{
    const jsonResponse = await axios.get(url2);
    const response = jsonResponse.data;

    Object.keys(response).forEach(key => {
      const poke= response[key]
      lista2.push(poke)
});
  console.log('', lista2, '');
  guardarInfo2(lista2)
    
    res.json({
      response
    });
  } catch (error) {
    console.log(error);
    
    res.json({
      error: 'Hubo un error al consumir el servicio'
    });
  }

};


const jsonPHGet = async (req, res = response) => {

  try{
    const jsonResponse = await axios.get(url3);
    const response = jsonResponse.data;

    Object.keys(response).forEach(key => {
      const poke= response[key]
      lista3.push(poke)
});
  console.log('', lista3, '');
  guardarInfo3(lista3)
    
    res.json({
      response
    });
  } catch (error) {
    console.log(error);
    
    res.json({
      error: 'ERROR EN SERVIDOR'
    });
  }

};


const personaPost = (req, res = response) => {
  res.json({
    msg: 'cargado pos',
    lista,
  });
};

const personaPut = (req, res = response) => {

  res.json({
    msg: 'cargado put'
  });
};

const personaDelete = (req, res = response) => {
  
  res.json({
    msg: 'cargado delete'
  });
};

module.exports = {
  pokemonAPiGet,
  covidTrackingGet,
  jsonPHGet,
  personaPut,
  personaPost,
  personaDelete
}