const express = require('express');
const axios = require('axios');

class Server {

  constructor(){
    this.app = express();
    this.port = process.env.PORT || 8080;
    this.personaPath = '/api/persona';
    this.middlewares();
    this.routes();
  }

  middlewares() {
    this.app.use(express.json())
    this.app.use(express.static('public'));
  }

   routes() {
     this.app.use(this.personaPath, require('../routes/personaRoutes'));
  }

   listen() {
    this.app.listen(this.port, () => {
    console.log('Servidor corriendo en puerto ', this.port);
    });
  }
}

module.exports = Server;