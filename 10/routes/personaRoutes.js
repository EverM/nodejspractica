const { Router } = require('express');
const {
  pokemonAPiGet,
  covidTrackingGet,
  jsonPHGet,
  personaPut,
  personaPost,
  personaDelete
} = require('../controllers/personaController');

const router = Router();

router.get('/1', pokemonAPiGet);
router.get('/2', covidTrackingGet);
router.get('/3', jsonPHGet);
router.put('/', personaPut);
router.post('/', personaPost);
router.delete('/', personaDelete);

module.exports = router;