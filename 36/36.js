/*
f(x) = { x/2 ; si x es par
         3x + 1; si x es impar
  
  Donde x es un elemento de un array que tiene 500.000 elementos, que son generados del 100 al 700.000
  de forma aleatoria, realizar la aplicación de la conjetura de collatz en cada elemento del array, la
  conjetura de collatz termina cuando el valor es igual a 1.
  Realizar la escritura en un archivo txt, dentro de una carpeta llamada salida-collatz que está en la raíz del
  proyecto. */
const fs = require('fs');

const generarList = () => {
  list= []
  for (let i = 0; i < 500000; i++) {
    list.push(random())
  }
  return list
}


const random = () => {
  return Math.floor((Math.random() * (700000 - 100 +1)) + 100);
}

const steps = (x) => {
  const munA = x 
  listaValores = []
  let contador = 0;
  if (x > 0 && Number.isInteger(x)) {
    while (x !== 1) {
      if (x % 2 === 0) {
        x = x / 2;
        contador++;
      } else if (x % 2 !== 0) {
        x = (x * 3) + 1;
        contador++;
      }
      listaValores.push(x);

    }
    return `Valor ${numA} : ${listaValores} \n`;
  }else {
    throw new Error(`El valor ingresado no es un numero entero ${x}`);
  }
};

const seveFile = (list) => fs.writeFileSync('salida-collatz/resultado.txt', list);

module.exports = {
  generar : generarList ,
  step: steps,
  save : seveFile
}



