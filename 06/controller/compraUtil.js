const { response, request } = 'express';
const DataCompraUtiles = require('../data/dataJson')
const CompraUtil = require('../models/compra-util')

const dataCompraUtiles = new DataCompraUtiles()

const compraUtilGet = (req, res = response) => {
  let data = dataCompraUtiles.getDB()

  res.json({
    msg: 'Datos optenidos con exito',
    data
  });
};

const compraUtilPut = (req = request, res = response) => {
  const { id } = req.params;

  const { material, precio_unitario, moneda } = req.body;
  
  const index = dataCompraUtiles.data.findIndex(object => object.id === id)

  if (index !== -1) {
    editada = dataCompraUtiles.editCompraUtiles({ material, precio_unitario, moneda }, index)
    res.json({
      msg: `Compra con el id: ${id} se modifico`,
      editada
    });
  }else {
    res.json({
      msg: `Compra con el id: ${id} no se encontro`,
    });
  }

};

const compraUtilPost = (req = request, res) => {
  const {material, precio_unitario, moneda} = req.body;
  nuevaCompraUtuiles = new CompraUtil(material, precio_unitario, moneda)
  dataCompraUtiles.addCompraUtil(nuevaCompraUtuiles)

  res.status(201).json({
    msg: 'Compra Agregada con exito',
    nuevaCompraUtuiles
  });
}

const compraUtilDelete = (req, res = response) => {
  
  const { id } = req.params;

  const index = dataCompraUtiles.data.findIndex(object => object.id === id)

  if (index !== -1) {
    let eliminada = dataCompraUtiles.deletCompraUtiles(index)
    res.json({
      msg: `Compra con el id: ${id} se elimino`,
      eliminada
    });
  } else {
    res.json({
      msg: 'Compra no encontrada'
    }); 
  }
  
};

module.exports = {
  compraUtilPost,
  compraUtilGet,
  compraUtilPut,
  compraUtilDelete
};
