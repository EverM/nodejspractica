const fs = require('fs');
const archivo ='./data/compra-utiles.json';

class DataCompraUtiles {

  constructor() {
    this.data = []
    this.leer = fs.readFileSync
    this.escribir = fs.writeFileSync
    this.cargarDatos()
  }

  cargarDatos = () => {
    if(!fs.existsSync(archivo) || this.data.length !== 0){
      return null;
    } else {
      let rawdata = this.leer(archivo);
      this.data = JSON.parse(rawdata);
    }
  }
  
  guardar = () => {  
    let data = JSON.stringify(this.data, null, 2);
    this.escribir(archivo, data);
  };

  addCompraUtil = (compraUtil) => {
    this.data.push(compraUtil)
    this.guardar()
  }
  
  getDB = () => {
    return this.data
  };

  editCompraUtiles = (CompraUtiles, index) => {
    this.data[index].material = CompraUtiles.material
    this.data[index].precio_unitario = CompraUtiles.precio_unitario
    this.data[index].moneda = CompraUtiles.moneda
    this.data[index].precio_total = CompraUtiles.precio_unitario["c-1"] + CompraUtiles.precio_unitario["c-2"]

    this.guardar();
    return this.data[index];
  }

  deletCompraUtiles = (id) => {
    let compraUtilesEliminada = this.data[id];
    this.data.splice(id, 1)

    this.guardar();
    return compraUtilesEliminada;
  }
}

module.exports = DataCompraUtiles
