const { response, request } = 'express';
const DataPerson = require('../data/dataJson')
const Persona = require('../models/persona')

const dataPeronas = new DataPerson()

//Get optional name and lastname
const personaGet = (req, res = response) => {
  var { nombres, apellidos } = req.query;
  if(nombres && apellidos){
    persona = dataPeronas.getByName(nombres)
    persona.concat(dataPeronas.getByLastname(apellidos))
    res.json({
      msg: 'Datos optenidos con exito',
      persona
    });
  } else if(nombres) {
    persona = dataPeronas.getByName(nombres)
    res.json({
      msg: 'Datos optenidos con exito',
      persona
    });
  } else if(apellidos) {
    persona = dataPeronas.getByLastname(apellidos)
    res.json({
      msg: 'Datos optenidos con exito',
      persona
    });
  } else {
    let personas = dataPeronas.getDB()
    res.json({
      msg: 'Datos optenidos con exito',
      personas
    });
  }
};

// Get by ci and Sex
const personaGetCiSex = (req, res = response) => {
  const { ciSex } = req.params;
  if (ciSex.toLowerCase() == 'masculino' || ciSex.toLowerCase() == 'femenino') {
    persona = dataPeronas.getBySex(ciSex.toLowerCase())
    res.json({
      msg: 'Datos optenidos con exito',
      persona
    });
  } else {
    let num = parseInt(ciSex);
    persona = dataPeronas.getByCi(num)
    res.json({
      msg: 'Persona encontrada',
      persona
    });
  }
}

// Get by Sex and Chart
const personaGetSexo = (req, res = response) => {
  const { x, sexo } = req.params;
  personas = dataPeronas.getBySexChart(x.toUpperCase(), sexo.toLowerCase())
  res.json({
    msg: 'Datos optenidos con exito',
    personas
  });
}

//Put
const personaPut = (req = request, res = response) => {
  const { id } = req.params;

  const { nombres, apellidos, ci, direccion, sexo } = req.body;
  
  const index = dataPeronas.data.findIndex(object => object.id === id)

  if (index !== -1) {
    personaEditada = dataPeronas.editPersona({ nombres, apellidos, ci, direccion, sexo }, index)
    res.json({
      msg: `Persona con el id: ${id} se modifico`,
      personaEditada
    });
  }else {
    res.json({
      msg: `Persona con el id: ${id} no se encontro`
    });
  }

};

//Post
const personaPost = (req = request, res) => {
  const { nombres, apellidos, ci, direccion, sexo } = req.body;
  nuevaPersona = new Persona(nombres, apellidos, ci, direccion, sexo)
  dataPeronas.addPersona(nuevaPersona)

  res.status(201).json({
    msg: 'Persona Agregada con exito',
    nuevaPersona
  });
}

//Deleted
const personaDelete = (req, res = response) => {
  
  const { id } = req.params;

  const index = dataPeronas.data.findIndex(object => object.id === id)

  if (index !== -1) {
    let personaEliminada = dataPeronas.deletPersona(index)
    res.json({
      msg: `Persona con el id: ${id} se elimino`,
      personaEliminada
    });
  } else {
    res.json({
      msg: 'Persona no encontrada'
    }); 
  }
  
};

module.exports = {
  personaPost,
  personaGet,
  personaPut,
  personaDelete,
  personaGetCiSex,
  personaGetSexo
};
